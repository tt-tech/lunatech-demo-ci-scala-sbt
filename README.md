# GitLab Continuous Integration (CI) Demo with Scala & SBT

[![build status](https://gitlab.com/jasperdenkers/lunatech-demo-ci-scala-sbt/badges/master/build.svg)](https://gitlab.com/jasperdenkers/lunatech-demo-ci-scala-sbt/commits/master) [![coverage report](https://gitlab.com/jasperdenkers/lunatech-demo-ci-scala-sbt/badges/master/coverage.svg)](https://gitlab.com/jasperdenkers/lunatech-demo-ci-scala-sbt/commits/master)

This is an example project for a post on the Lunatech blog that demonstrates GitLab's CI functionalities with Scala & SBT.

The post can be found here: http://lunatech.com/blog/V5tpoCQAANsPL4jw/continuous-integration-on-gitlab-with-scala-and-sbt.

## Getting Started

If you have [SBT](http://www.scala-sbt.org/) installed and this project checked out locally on your machine, you can execute the following commands in the root directory:

 - `sbt run`: Run the project.
 - `sbt test`: Execute the project's tests.
 - `sbt clean coverage test coverageReport`: Generate a test coverage report.

## Test Coverage in GitLab

To display the test coverage next to build results in GitLab you have to add the following regex in your project's settings (under **CI/CD Pipelines** > **Test coverage parsing**): `Coverage was \[\d+.\d+\%\]`.
